import os
from pathlib import Path

services = {"bos", "catman", "subren", "subman", "oss", "dpes"}
for path in Path(".").glob("services/*/CHANGES.txt"):
    service = path.parts[1]
    with open(path) as fp:
        version = next(fp).split()[-1].strip()
    if service not in services:
        continue
    print(f"Checking for changes in {service} {version}")
    minor_version = version.rsplit('.', 1)[0]
    branch = f"release-{service}-{minor_version}"
    print(f"branch---> {branch}")
    resp = os.system(f"git fetch && git branch -a | grep remotes/origin/{branch}$ 2>&1 > /dev/null")
    print(f"resp---> {resp}")
    if resp != 0:
        print(f"creando rama---> {branch}")
        os.system(f"git config remote.origin.url 'https://token-jenkins:glpat-szUxt3WZxxqs6y6cJur6@gitlab.com/lennux23/test-jenkins.git' && git config remote.origin.fetch +refs/heads/*:refs/remotes/origin/*") 
        os.system(f"git checkout -b {branch} && git push --set-upstream origin {branch}")
        '''os.system(f"curl -k https://jenkins.prod.rpay.int.roku.com/job/RPAY-DEPLOY-MONOREPO-SERVICE-jjb-0-QA/")'''
